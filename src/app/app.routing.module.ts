import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { UserComponent } from "./user/user.component";
import { ProjectComponent } from "./project/project.component";
import { TaskComponent } from "./task/task.component";
const routes: Routes = [
  { path: "user", component: UserComponent },
  { path: "project", component: ProjectComponent },
  { path: "task", component: TaskComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
