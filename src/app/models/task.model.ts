import { ParentTask } from "./parentTask.model";
import { User } from "./user.model";
import { Project } from "./project.model";

export class Task {
	taskId: number;
	task: string;
	priority: number;
	parentTask: ParentTask;
	user: User;
	project: Project;
	isCompleted: boolean;
	startDate: Date;
	endDate: Date;
}
