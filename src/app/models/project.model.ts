import { User } from "./user.model";

export class Project {
    projectId: number;
    projectName: string;
    startDate: Date;
    endDate: Date;
    priority: number;
    manager: User;
    isCompleted: boolean;
    numberofTasks: number;
    completedTasks: number;
}

