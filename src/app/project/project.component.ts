import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { ProjectService } from "../service/project.service";
import { UserService } from "../service/user.service";
import { Project } from "../models/project.model";
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ModalDirective } from "angular-bootstrap-md";
import { DatePipe } from '@angular/common';
import { User } from "../models/user.model";
import { NotificationService } from "../service/notification.service";

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {
  @ViewChild('closeBtn') closeBtn: ElementRef;
  @ViewChild('frame') frame: ModalDirective;

  projectForm: FormGroup;
  data;
  filterData;
  userData;
  selectedPriority: number;
  isSubmitDisabled = false;
  isUpdateDisabled = true;
  title = "Add";
  managerPlaceholder = "Select Manager";
  public formStartDate: Date;
  public formEndDate: Date;
  fromDateCheck = false;
  toDateCheck = false;
  priorityCheck = false;
  userCheck = false;
  validateField = false;

  constructor(
    private notifyService: NotificationService,
    private projectService: ProjectService,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private datePipe: DatePipe) {
    this.projectForm = this.createFormGroup(formBuilder);
  }

  get form() { return this.projectForm.controls; }

  createFormGroup(formBuilder: FormBuilder) {
    console.log(this.selectedPriority);
    this.formStartDate;
    this.formEndDate;
    this.selectedPriority = 0;
    this.validateField = false;
    this.managerPlaceholder = "Select Manager";
    return formBuilder.group({
      projectName: new FormControl('', Validators.required),
      startDate: new FormControl(null, Validators.required),
      endDate: new FormControl(null, Validators.required),
      priority: new FormControl('', Validators.required),
      manager: ({
        userId: new FormControl('', Validators.required)
      })
    });
  }

  ngOnInit() {
    this.getData();
    this.getUserData();
  }
  showToaster(message) {
    this.notifyService.showSuccess(message, "Project Info");
  }

  serverMessage() {
    this.notifyService.showError("Please Try Again ", "Server Error");
  }
  onUserChange(user: User) {
    console.log(user);
  }
  addProjectData() {
    this.projectForm = this.createFormGroup(this.formBuilder);
    this.isSubmitDisabled = false;
    this.isUpdateDisabled = true;
    this.title = "Add";
    this.revert();
  }

  updateProjectData(project: Project) {
    this.projectForm = this.createEditFormGroup(this.formBuilder, project);
    this.isSubmitDisabled = true;
    this.isUpdateDisabled = false;
    this.title = "Update";
    this.fromDateCheck = false;
    this.toDateCheck = false;
  }

  createEditFormGroup(formBuilder: FormBuilder, project: Project) {
    this.validateField = true;
    this.formStartDate = project.startDate;
    this.formEndDate = project.endDate;
    this.selectedPriority = project.priority;
    this.managerPlaceholder = project.manager.firstName;
    return formBuilder.group({
      projectId: project.projectId,
      projectName: project.projectName,
      startDate: project.startDate,
      endDate: project.endDate,
      priority: project.priority,
      manager: formBuilder.group({
        userId: project.manager.userId
      })
    });
  }

  revert() {
    // Resets to blank object
    // Resets to provided model
    this.selectedPriority = 0;
    this.formStartDate = null;
    this.formEndDate = null;
    this.fromDateCheck = false;
    this.toDateCheck = false;
    this.priorityCheck = false;
    this.userCheck = false;
    this.validateField = false;
    if (this.title == "Add") {
      this.projectForm = this.createFormGroup(this.formBuilder);
    }
    else {
      this.selectedPriority = 0;
      this.projectForm.patchValue({
        projectName: null,
        startDate: null,
        endDate: null,
        priority: null,
        manager: ({
          userId: null
        })
      });
    }
  }

  getData(): void {
    this.projectService.getProject().subscribe(res => {
      this.data = res;
      this.filterData = res;
    },
      err => {
        console.log("Error occured");
        this.notifyService.showError("Unable to fetch project details. Server is down", "Server Error");
      }
    );

    console.log("Get Data" + JSON.stringify(this.filterData));
  }


  getUserData(): void {
    this.userService.getUser().subscribe(res => {
      this.userData = res;
    },
      err => {
        console.log("Error occured");
        this.notifyService.showError("Unable to fetch user details. Server is down", "Server Error");
      }
    );

    console.log("Get Data" + JSON.stringify(this.userData));
  }

  getFormatdate(changeDate: Date): any {
    return this.datePipe.transform(changeDate, "yyyy-MM-dd");
  }

  updateProject() {
    if (this.projectForm.invalid || this.projectForm.value.projectName == null || this.projectForm.value.startDate == null ||
      this.projectForm.value.endDate == null || this.projectForm.value.priority == null || this.projectForm.value.manager.userId == null) {
      this.notifyService.showWarning("Please fill the required fields", "Project Info");
      return;
    }
    const project: Project = Object.assign({}, this.projectForm.value);

    // Do useful stuff with the gathered data
    console.log("Update Project");
    console.log(project);
    this.projectService.updateProject(project).subscribe(data => {
      this.getData();
      this.showToaster(project.projectName + " updated successfully");

    });
    this.frame.hide();
    this.revert();
  }


  deleteProject(project: Project): void {
    this.projectService.deleteProject(project).subscribe(

      data => {
        this.getData();
        this.showToaster(project.projectName + " deleted successfully");

      },
      err => {
        console.log("Error occured");
        this.serverMessage();
      }
    );
  }

  onSubmit() {
    if (this.projectForm.invalid || (this.userCheck == false)) {
      this.notifyService.showWarning("Please fill the required fields", "User Info");
      return;
    }
    const projectData: Project = Object.assign({}, this.projectForm.value);
    // Do useful stuff with the gathered data
    console.log(projectData);

    this.projectService.createProject(projectData).subscribe(data => {
      console.log(projectData);
      this.ngOnInit();
      this.showToaster(projectData.projectName + " created successfully");
    },
      err => {
        console.log("Error occured");
        this.serverMessage();
      }

    );
    this.closeBtn.nativeElement.click();
    this.frame.hide();
    this.revert();
    this.projectForm.reset();
  }


  changeValue(e) {
    console.log(e.newValue);
    this.selectedPriority = e.newValue;
    this.projectForm.patchValue({
      priority: this.selectedPriority
    });
    this.priorityCheck = true;
    if (e.newValue == 0) {
      this.priorityCheck = false;
    }
  }

  getUserValue(e) {
    console.log(e);
    this.projectForm.patchValue({
      manager: ({
        userId: e.userId
      })
    });
    this.userCheck = true;
    if (e.userId == null) {
      this.userCheck = false;
    }
  }
  changeStartDate(e) {
    console.log(e.value);
    var start = this.getFormatdate(e.value);
    this.projectForm.patchValue({
      startDate: start
    });
    this.fromDateCheck = true;
    if (e.value == null) {
      this.fromDateCheck = false;
    }
  }

  changeEndDate(e) {
    console.log(e.value);
    var end = this.getFormatdate(e.value);
    this.projectForm.patchValue({
      endDate: end
    });
    this.toDateCheck = true;
    if (e.value == null) {
      this.toDateCheck = false;
    }
  }
  search(term: string) {
    if (!term) {
      this.filterData = this.data;
      console.log("Filter Data before Search" + JSON.stringify(this.filterData));
    } else {
      this.filterData = this.data.filter(
        x =>
          x.projectName
            .trim()
            .toLowerCase()
            .includes(term.trim().toLowerCase()) ||
          x.numberofTasks.toString()
            .trim()
            .toLowerCase()
            .includes(term.trim().toLowerCase()) ||
          x.completedTasks.toString()
            .trim()
            .toLowerCase()
            .includes(term.trim().toLowerCase()) ||
          x.manager.firstName
            .trim()
            .toLowerCase()
            .includes(term.trim().toLowerCase()) ||
          x.manager.lastName
            .trim()
            .toLowerCase()
            .includes(term.trim().toLowerCase()) ||
          x.startDate
            .trim()
            .toLowerCase()
            .includes(term.trim().toLowerCase()) ||
          x.endDate
            .trim()
            .toLowerCase()
            .includes(term.trim().toLowerCase()) ||
          x.priority.toString()
            .trim()
            .toLowerCase()
            .includes(term.trim().toLowerCase())
      );
      console.log("Filter Data after Search" + JSON.stringify(this.filterData));
    }
  }

  endProject(project: Project): void {
    console.log("end");
    project.isCompleted = true;
    console.log(project);
    this.projectService.updateEndProject(project).subscribe(data => {
      console.log(project.projectName + " has been ended");
      this.showToaster(project.projectName + " has been ended");
    },
      err => {
        console.log("Error occured");
        this.serverMessage();
      }
    );
  }
}
