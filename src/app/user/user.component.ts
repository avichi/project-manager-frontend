import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { UserService } from "../service/user.service";
import { User } from "../models/user.model";
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ModalDirective } from "angular-bootstrap-md";
import { NotificationService } from "../service/notification.service";

@Component({
  selector: "app-user",
  templateUrl: "./user.component.html",
  styleUrls: ["./user.component.scss"]
})
export class UserComponent implements OnInit {
  @ViewChild('closeBtn') closeBtn: ElementRef;
  @ViewChild('frame') frame: ModalDirective;

  userForm: FormGroup;
  data;
  filterData;
  userData: User;
  isSubmitDisabled = false;
  isUpdateDisabled = true;
  title = "Add";
  constructor(
    private notifyService: NotificationService,
    private userService: UserService,
    private formBuilder: FormBuilder) {
    this.userForm = this.createFormGroup(formBuilder);
  }

  ngOnInit() {
    this.getData();
  }

  showToaster(message) {
    this.notifyService.showSuccess(message, "User Info");
  }

  serverMessage() {
    this.notifyService.showError("Please Try Again ", "Server Error");
  }

  addUserData() {
    this.userForm = this.createFormGroup(this.formBuilder);
    this.isSubmitDisabled = false;
    this.isUpdateDisabled = true;
    this.title = "Add";
  }

  updateUserData(user: User) {
    this.userForm = this.createEditFormGroup(this.formBuilder, user);
    this.isSubmitDisabled = true;
    this.isUpdateDisabled = false;
    this.title = "Update";
  }

  get form() { return this.userForm.controls; }

  createFormGroup(formBuilder: FormBuilder) {
    return formBuilder.group({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      employeeId: new FormControl('', Validators.required)
    });
  }

  createEditFormGroup(formBuilder: FormBuilder, user: User) {
    return formBuilder.group({
      userId: user.userId,
      firstName: user.firstName,
      lastName: user.lastName,
      employeeId: user.employeeId
    });
  }

  revert() {
    // Resets to blank object
    // Resets to provided model

    if (this.title == "Add") { this.userForm = this.createFormGroup(this.formBuilder); }
    else {
      this.userForm.patchValue({
        firstName: null,
        lastName: null,
        employeeId: null
      });
    }
  }

  onSubmit() {
    if (this.userForm.invalid) {
      this.notifyService.showWarning("Please fill the required fields", "User Info");
      return;
    }
    // Make sure to create a deep copy of the form-model
    const user: User = Object.assign({}, this.userForm.value);

    // Do useful stuff with the gathered data
    console.log(user);
    this.userService.createUser(user).subscribe(data => {
      console.log(user);
      this.ngOnInit();
      this.showToaster(user.firstName + " " + user.lastName + " created successfully");
    },
      err => {
        console.log("Error occured");
        this.serverMessage();
      }
    );
    this.revert();
    this.closeBtn.nativeElement.click();
    this.frame.hide();
  }

  updateUser() {
    if (this.userForm.invalid || this.userForm.value.firstName == null ||
      this.userForm.value.lastName == null || this.userForm.value.employeeId == null) {
      this.notifyService.showWarning("Please fill the required fields", "User Info");
      return;
    }
    const user: User = Object.assign({}, this.userForm.value);

    // Do useful stuff with the gathered data
    console.log("Update User");
    console.log(user);
    this.userService.updateUser(user).subscribe(data => {
      this.getData();
      this.showToaster(user.firstName + " " + user.lastName + " updated successfully");
    },
      err => {
        console.log("Error occured");
        this.serverMessage();
      }
    );
    this.frame.hide();
  }

  deleteUser(user: User): void {
    this.userService.deleteUser(user).subscribe(

      data => {
        this.getData();
        this.showToaster(user.firstName + " " + user.lastName + " deleted successfully");
      },
      err => {
        console.log("Error occured");
        this.serverMessage();
      }
    );
  }

  getData(): void {
    this.userService.getUser().subscribe(res => {
      this.data = res;
      this.filterData = res;
    },
      err => {
        console.log("Error occured");
        this.notifyService.showError("Please try again later. Server is down", "Server Error");
      }
    );

    console.log("Get Data" + JSON.stringify(this.filterData));
  }

  search(term: string) {
    if (!term) {
      this.filterData = this.data;
      console.log("Filter Data before Search" + JSON.stringify(this.filterData));
    } else {
      this.filterData = this.data.filter(
        x =>
          x.firstName
            .trim()
            .toLowerCase()
            .includes(term.trim().toLowerCase()) ||
          x.lastName
            .trim()
            .toLowerCase()
            .includes(term.trim().toLowerCase()) ||
          x.employeeId
            .trim()
            .toLowerCase()
            .includes(term.trim().toLowerCase())
      );
      console.log("Filter Data after Search" + JSON.stringify(this.filterData));
    }
  }
}
