import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppComponent } from "./app.component";
import { UserService } from "./service/user.service";
import { ProjectService } from "./service/project.service";
import { TaskService } from "./service/task.service";
import { AppRoutingModule } from "./app.routing.module";
import { MDBBootstrapModule } from "angular-bootstrap-md";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { UserComponent } from "./user/user.component";
import { ProjectComponent } from "./project/project.component";
import { TaskComponent } from "./task/task.component";
import { NavigationComponent } from "./navigation/navigation.component";
import { DataTableModule } from "angular-6-datatable";
import { HttpClientModule } from "@angular/common/http";
import { DatePickerModule } from "@syncfusion/ej2-angular-calendars";
import { DatePipe } from '@angular/common';
import { NgxBootstrapSliderModule } from 'ngx-bootstrap-slider';
import { SuiModule } from 'ng2-semantic-ui';
import { NotificationService } from "./service/notification.service";
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [AppComponent, UserComponent, NavigationComponent, ProjectComponent, TaskComponent],
  imports: [
    BrowserModule,
    DatePickerModule,
    DataTableModule,
    BrowserAnimationsModule,
    MDBBootstrapModule.forRoot(),
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxBootstrapSliderModule,
    SuiModule,
    ToastrModule.forRoot()
  ],
  providers: [NotificationService, UserService, ProjectService, TaskService, DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
