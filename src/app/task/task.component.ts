import { TaskService } from './../service/task.service';
import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { ProjectService } from "../service/project.service";
import { UserService } from "../service/user.service";
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ModalDirective } from "angular-bootstrap-md";
import { DatePipe } from '@angular/common';
import { Task } from "../models/task.model";
import { NotificationService } from "../service/notification.service";

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {
  @ViewChild('closeBtn') closeBtn: ElementRef;
  @ViewChild('frame') frame: ModalDirective;

  taskForm: FormGroup;
  data;
  filterData;
  userData;
  projectData;
  parentTaskData;
  selectedPriority: number;
  isSubmitDisabled = false;
  isUpdateDisabled = true;
  isParentTaskDisabled = true;
  title = "Add";
  userPlaceholder = "Select User";
  projectPlaceholder = "Select Project";
  taskPlaceholder = "Select Parent Task";
  public formStartDate: Date;
  public formEndDate: Date;
  fromDateCheck = false;
  toDateCheck = false;
  priorityCheck = false;
  userCheck = false;
  projectCheck = false;
  validateField = false;

  constructor(
    private notifyService: NotificationService,
    private projectService: ProjectService,
    private userService: UserService,
    private taskService: TaskService,
    private formBuilder: FormBuilder,
    private datePipe: DatePipe) {
    this.taskForm = this.createFormGroup(formBuilder);
  }

  ngOnInit() {
    this.getData();
    this.getProjectData();
    this.getUserData();
  }
  showToaster(message) {
    this.notifyService.showSuccess(message, "Task Info");
  }

  serverMessage() {
    this.notifyService.showError("Please Try Again ", "Server Error");
  }

  toggleVisibility(e) {
    console.log(this.isParentTaskDisabled);
    this.isParentTaskDisabled = !e.checked;
    console.log(this.isParentTaskDisabled);
  }

  get form() { return this.taskForm.controls; }

  createFormGroup(formBuilder: FormBuilder) {
    console.log(this.selectedPriority);
    this.formStartDate;
    this.formEndDate;
    this.selectedPriority = 0;
    this.validateField = false;
    this.userPlaceholder = "Select User";
    this.projectPlaceholder = "Select Project";
    this.taskPlaceholder = "Select Parent Task";
    return formBuilder.group({
      task: new FormControl('', Validators.required),
      startDate: new FormControl(null, Validators.required),
      endDate: new FormControl(null, Validators.required),
      priority: new FormControl('', Validators.required),
      user: ({
        userId: new FormControl('', Validators.required),
      }),
      project: ({
        projectId: new FormControl('', Validators.required),
      }),
      parentTask: ({
        parentTaskName: ''
      })
    });
  }

  createEditFormGroup(formBuilder: FormBuilder, task: Task) {
    this.validateField = true;
    this.formStartDate = task.startDate;
    this.formEndDate = task.endDate;
    this.selectedPriority = task.priority;
    this.userPlaceholder = task.user.firstName;
    this.taskPlaceholder = task.parentTask.parentTaskName;
    this.projectPlaceholder = task.project.projectName;
    return formBuilder.group({
      taskId: task.taskId,
      task: task.task,
      startDate: task.startDate,
      endDate: task.endDate,
      priority: task.priority,
      user: formBuilder.group({
        userId: task.user.userId
      }),
      project: formBuilder.group({
        projectId: task.project.projectId
      }),
      parentTask: formBuilder.group({
        parentTaskName: task.parentTask.parentTaskName
      })
    });
  }

  addTaskData() {
    this.taskForm = this.createFormGroup(this.formBuilder);
    this.isSubmitDisabled = false;
    this.isUpdateDisabled = true;
    this.title = "Add";
    this.revert();
  }

  updateTaskData(task: Task) {
    this.taskForm = this.createEditFormGroup(this.formBuilder, task);
    this.isSubmitDisabled = true;
    this.isUpdateDisabled = false;
    this.title = "Update";
    this.fromDateCheck = false;
    this.toDateCheck = false;
  }



  revert() {
    // Resets to blank object
    // Resets to provided model
    this.selectedPriority = 0;
    this.formStartDate = null;
    this.formEndDate = null;
    this.fromDateCheck = false;
    this.toDateCheck = false;
    this.priorityCheck = false;
    this.userCheck = false;
    this.projectCheck = false;
    this.validateField = false;
    if (this.title == "Add") {
      this.taskForm = this.createFormGroup(this.formBuilder);
    }
    else {
      this.taskForm.patchValue({
        task: null,
        startDate: null,
        endDate: null,
        priority: null,
        user: ({
          userId: null,
        }),
        project: ({
          projectId: null,
        }),
        parentTask: ({
          parentTaskName: null
        })
      });
    }
  }

  getData(): void {
    this.taskService.getTask().subscribe(res => {
      this.data = res;
      this.filterData = res;
      console.log("Get Data" + JSON.stringify(this.filterData));
    },
      err => {
        console.log("Error occured");
        this.notifyService.showError("Unable to fetch task details. Server is down", "Server Error");
      }
    );
  }



  getProjectData(): void {
    this.projectService.getProject().subscribe(res => {
      this.projectData = res;
      console.log("Get Data" + JSON.stringify(this.filterData));
    },
      err => {
        console.log("Error occured");
        this.notifyService.showError("Unable to fetch project details. Server is down", "Server Error");
      }
    );

  }


  getUserData(): void {
    this.userService.getUser().subscribe(res => {
      this.userData = res;
      console.log("Get Data" + JSON.stringify(this.userData));
    },
      err => {
        console.log("Error occured");
        this.notifyService.showError("Unable to fetch user details. Server is down", "Server Error");
      }
    );

  }

  getFormatdate(changeDate: Date): any {
    return this.datePipe.transform(changeDate, "yyyy-MM-dd");
  }

  updateTask() {
    if (this.taskForm.invalid || this.taskForm.value.task == null || this.taskForm.value.startDate == null ||
      this.taskForm.value.endDate == null || this.taskForm.value.priority == null || this.taskForm.value.user.userId == null ||
      this.taskForm.value.project.projectId == null) {
      this.notifyService.showWarning("Please fill the required fields", "User Info");
      return;
    }
    const task: Task = Object.assign({}, this.taskForm.value);

    // Do useful stuff with the gathered data
    console.log("Update Task");
    console.log(task);
    this.taskService.updateTask(task).subscribe(data => {
      this.getData();
      this.showToaster(task.task + " updated successfully");
    },
      err => {
        console.log("Error occured");
        this.serverMessage();
      });
    this.frame.hide();
    this.revert();
  }


  deleteTask(task: Task): void {
    this.taskService.deleteTask(task).subscribe(

      data => {
        this.getData();
        this.showToaster(task.task + " deleted successfully");

      },
      err => {
        console.log("Error occured");
        this.serverMessage();
      }
    );
  }

  onSubmit() {

    if ((this.userCheck == false) || (this.projectCheck == false)) {
      this.notifyService.showWarning("Please fill the required fields", "User Info");
      return;
    }
    if (this.taskForm.invalid) {
      this.notifyService.showWarning("Please fill the required fields", "User Info");
      return;
    }
    const taskData: Task = Object.assign({}, this.taskForm.value);
    // Do useful stuff with the gathered data
    console.log(taskData);

    this.taskService.createTask(taskData).subscribe(data => {
      console.log(taskData);
      this.ngOnInit();
      this.showToaster(taskData.task + " created successfully");
    },
      err => {
        console.log("Error occured");
        this.serverMessage();
      });
    this.closeBtn.nativeElement.click();
    this.frame.hide();
    this.revert();
    this.taskForm.reset();
  }


  changeValue(e) {
    console.log(e.newValue);
    this.selectedPriority = e.newValue;
    this.taskForm.patchValue({
      priority: this.selectedPriority
    });
    this.priorityCheck = true;
    if (e.newValue == 0) {
      this.priorityCheck = false;
    }
  }
  getUserValue(e) {
    console.log(e);
    this.taskForm.patchValue({
      user: ({
        userId: e.userId
      })
    });
    this.userCheck = true;
    if (e.userId == null) {
      this.userCheck = false;
    }
  }
  getProjectValue(e) {
    console.log(e);
    this.taskForm.patchValue({
      project: ({
        projectId: e.projectId
      })
    });
    this.projectCheck = true;
    if (e.projectId == null) {
      this.projectCheck = false;
    }
  }
  getParentTaskValue(e) {
    console.log(e);
    this.taskForm.patchValue({
      parentTask: ({
        parentTaskName: e.task
      })
    });
  }
  changeStartDate(e) {
    console.log(e.value);
    const start = this.getFormatdate(e.value);
    this.taskForm.patchValue({
      startDate: start
    });
    this.fromDateCheck = true;
    if (e.value == null) {
      this.fromDateCheck = false;
    }
  }

  changeEndDate(e) {
    console.log(e.value);
    const end = this.getFormatdate(e.value);
    this.taskForm.patchValue({
      endDate: end
    });
    this.toDateCheck = true;
    if (e.value == null) {
      this.toDateCheck = false;
    }
  }
  search(term: string) {
    if (!term) {
      this.filterData = this.data;
      console.log("Filter Data before Search" + JSON.stringify(this.filterData));
    } else {
      this.filterData = this.data.filter(
        x =>
          x.task
            .trim()
            .toLowerCase()
            .includes(term.trim().toLowerCase()) ||
          x.project.projectName
            .trim()
            .toLowerCase()
            .includes(term.trim().toLowerCase()) ||
          x.user.firstName
            .trim()
            .toLowerCase()
            .includes(term.trim().toLowerCase()) ||
          x.parentTask.parentTaskName
            .trim()
            .toLowerCase()
            .includes(term.trim().toLowerCase()) ||
          x.startDate
            .trim()
            .toLowerCase()
            .includes(term.trim().toLowerCase()) ||
          x.endDate
            .trim()
            .toLowerCase()
            .includes(term.trim().toLowerCase()) ||
          x.priority.toString()
            .trim()
            .toLowerCase()
            .includes(term.trim().toLowerCase())
      );
      console.log("Filter Data after Search" + JSON.stringify(this.filterData));
    }
  }

  endTask(task: Task): void {
    console.log("end");
    task.isCompleted = true;
    console.log(task);
    this.taskService.updateEndTask(task).subscribe(data => {
      console.log(task.task + " has been ended");
      this.showToaster(task.task + " has been ended");

    },
      err => {
        console.log("Error occured");
        this.serverMessage();
      }
    );
  }
}
